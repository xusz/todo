package gqlgen_todos

import (
	fmt "fmt"
	io "io"
	math "math"

	proto "github.com/golang/protobuf/proto"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// UnmarshalGQL  TodoSortType graphql unmarshal
func (c *TodoSortType) UnmarshalGQL(v interface{}) error {
	code, ok := v.(string)
	if ok {
		*c = TodoSortType(TodoSortType_value[code])
		return nil
	}
	return fmt.Errorf("cannot unmarshal SearchType enum")
}

// MarshalGQL  TodoSortType graphql marshal
func (c TodoSortType) MarshalGQL(w io.Writer) {
	fmt.Fprintf(w, "%q", c.String())
}

// UnmarshalGQL  TodoPermissionRole graphql unmarshal
func (c *TodoPermissionRole) UnmarshalGQL(v interface{}) error {
	code, ok := v.(string)
	if ok {
		*c = TodoPermissionRole(TodoPermissionRole_value[code])
		return nil
	}
	return fmt.Errorf("cannot unmarshal SearchType enum")
}

// MarshalGQL  TodoPermissionRole graphql marshal
func (c TodoPermissionRole) MarshalGQL(w io.Writer) {
	fmt.Fprintf(w, "%q", c.String())
}
