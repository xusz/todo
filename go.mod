module gitlab.com/xusz/todo

go 1.13

require (
	github.com/golang/protobuf v1.4.2
	github.com/jinzhu/gorm v1.9.15
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/viper v1.7.0
	github.com/twitchtv/twirp v5.12.1+incompatible
	gitlab.com/momentum-valley/adam v0.2.78-beta
)
