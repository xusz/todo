package mysql_engine

import (
	"github.com/jinzhu/gorm"
)

// Engine ...
type Engine struct {
	DB *gorm.DB
}

// New ...
func New(database *gorm.DB) *Engine {
	return &Engine{DB: database}
}

// GetUserTodoDB ...
func (e *Engine) GetUserTodoDB(todoID, userID string) *gorm.DB {
	return e.DB.Table("todos t").
		Joins("inner join todo_users tu on t.id = tu.todo_id").
		Joins("inner join users u on tu.user_id = u.id").
		Select("t.*, tu.*, u.*").
		Where("t.id = ? AND tu.user_id = ?", todoID, userID).
		Where("t.deleted_at is null").
		Where("tu.deleted_at is null")
}
