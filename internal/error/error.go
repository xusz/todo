package error

import (
	"encoding/json"
)

// TodoError ...
type TodoError struct {
	Code    TodoErrorCode
	Message string
}

// TodoErrorCode ...
type TodoErrorCode string

// GetMessage ...
func (e TodoErrorCode) GetMessage() string {
	switch e {
	case ErrorCodeSystemWrong:
		return "系统错误"
	case ErrorCodeDataBaseWrong:
		return "数据库错误"
	case ErrorCodeUserNotExistence:
		return "账号不存在"
	case ErrorCodeUserNameAlreadyExistence:
		return "用户名称已存在，请更改名称后重试"
	case ErrorCodeAccountNoPermission:
		return "用户权限不足"
	case ErrorCodeDataNotExistence:
		return "备忘录 ID 不存在"
	case ErrorCodeTodoNameExistence:
		return "备忘录名称已存在，请更改名称后重试"
	case ErrorCodeTodoNameNotValid:
		return "备忘录名称不合理，请更改名称后重试"
	case ErrorCodeTodoNameTooLong:
		return "备忘录名称过长，请更改名称后重试"
	case ErrorCodeInputParameterNullError:
		return "未输入参数"
	default:
		return "系统异常"
	}
}

// GetError ...
func (e TodoErrorCode) GetError() TodoError {
	return TodoError{
		Code:    e,
		Message: e.GetMessage(),
	}
}

const (
	// ErrorCodeSystemWrong 系统错误
	ErrorCodeSystemWrong TodoErrorCode = "000000"
	// ErrorCodeDataBaseWrong 数据库错误
	ErrorCodeDataBaseWrong TodoErrorCode = "000001"

	// ErrorCodeUserNotExistence 账号不存在
	ErrorCodeUserNotExistence TodoErrorCode = "000002"
	// ErrorCodeUserNameAlreadyExistence 用户名称已存在
	ErrorCodeUserNameAlreadyExistence TodoErrorCode = "000003"

	// ErrorCodeAccountNoPermission 用户权限不足
	ErrorCodeAccountNoPermission TodoErrorCode = "000004"
	// ErrorCodeDataNotExistence 数据 ID 不存在
	ErrorCodeDataNotExistence TodoErrorCode = "000005"
	// ErrorCodeTodoNameExistence 备忘录名称已存在
	ErrorCodeTodoNameExistence TodoErrorCode = "000006"
	// ErrorCodeTodoNameNotValid 备忘录名称不合理，请重试
	ErrorCodeTodoNameNotValid TodoErrorCode = "000007"
	// ErrorCodeTodoNameTooLong 备忘录名称过长，请重试
	ErrorCodeTodoNameTooLong TodoErrorCode = "000008"
	// ErrorCodeInputParameterNullError 输入参数为空
	ErrorCodeInputParameterNullError TodoErrorCode = "000009"
)

func (e TodoError) Error() string {
	message, _ := json.Marshal(e)
	return string(message)
}
