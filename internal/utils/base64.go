package utils

import (
	"encoding/base64"
	"strconv"
)

// Base64Int ...
func Base64Int(source int) string {
	return base64.StdEncoding.EncodeToString([]byte(strconv.Itoa(source)))
}

// DecodeBase64ToInt ...
func DecodeBase64ToInt(source string) (int, error) {
	decodeBytes, err := base64.StdEncoding.DecodeString(source)
	if err != nil {
		return 0, err
	}
	result, err := strconv.Atoi(string(decodeBytes))
	if err != nil {
		return 0, err
	}
	return result, nil
}
