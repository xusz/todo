package utils

import (
	"encoding/json"
)

// LogJSONData ...
func LogJSONData(data interface{}) string {
	if !CONFIG.DebugMode {
		return ""
	}
	raw, err := json.Marshal(data)
	if err != nil {
		Logger.Println(err)
		return ""
	}
	return string(raw)
}
