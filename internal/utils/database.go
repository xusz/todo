package utils

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql" // db driver
)

// ConnectToDatabase ...
func ConnectToDatabase(config *Configuration) (*gorm.DB, error) {

	dbConnStr := fmt.Sprintf("%s:%s@(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local&multiStatements=True",
		config.DB.User, config.DB.Password, config.DB.Host, config.DB.Port, config.DB.DBName)

	db, err := gorm.Open(config.DB.DBType, dbConnStr)
	if err != nil {
		return nil, err
	}

	db.DB().SetMaxIdleConns(50)
	db.DB().SetMaxOpenConns(100)

	Logger.Println("connected to database...")

	return db, nil
}
