package utils

import (
	"errors"

	"github.com/golang/protobuf/ptypes/wrappers"
	"gitlab.com/momentum-valley/adam/rpc/common"
)

// GetOffsetAndLimit get pagination from two input pagination group: first/after, last/before
func GetOffsetAndLimit(first, last *wrappers.Int32Value, before, after *wrappers.StringValue) (offset, limit int, err error) {
	// pagination group cannot be nil or not nil at the same time
	if (first == nil && last == nil) || (first != nil && last != nil) || (before != nil && after != nil) {
		Logger.Error("invalid pagination")
		return 0, 0, errors.New("invalid pagination")
	}

	// cross validation of pagination group
	if (first != nil && before != nil) || (last != nil && after != nil) {
		Logger.Error("invalid pagination")
		return 0, 0, errors.New("invalid pagination")
	}

	limit = int(first.GetValue() + last.GetValue())
	cursor := before.GetValue() + after.GetValue()

	if limit <= 0 {
		Logger.Error("invalid pagination size")
		return 0, 0, errors.New("invalid pagination size")
	}

	if cursor != "" {
		var i int
		i, err = DecodeBase64ToInt(cursor)
		if err != nil {
			Logger.Error(err)
			return 0, 0, err
		}
		if after != nil {
			offset = i + 1
		}
		if before != nil {
			offset = i - limit
			if offset < 0 {
				offset = 0
			}
		}
	}
	return
}

// GetPaginationFromData get pagination from return data
func GetPaginationFromData(totalCount, offset, limit int) (*common.PageInfo, error) {
	if totalCount <= 0 {
		Logger.Infoln("pagination totalCount is no positive")

		return &common.PageInfo{
			HasPreviousPage: false,
			HasNextPage:     false,
			StartCursor:     nil,
			EndCursor:       nil,
		}, nil
	}

	endIndex := offset + limit - 1
	if offset+limit > totalCount {
		endIndex = totalCount - 1
	}

	pageInfo := common.PageInfo{
		HasPreviousPage: offset > 0,
		HasNextPage:     offset+limit < totalCount,
		StartCursor: &wrappers.StringValue{
			Value: Base64Int(offset),
		},
		EndCursor: &wrappers.StringValue{
			Value: Base64Int(endIndex),
		},
	}

	return &pageInfo, nil
}
