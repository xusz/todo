package simple

import (
	"context"

	"github.com/jinzhu/gorm"
	"gitlab.com/xusz/todo/internal/models"
	mysql_engine "gitlab.com/xusz/todo/internal/mysql-engine"
	"gitlab.com/xusz/todo/internal/storage"
	"gitlab.com/xusz/todo/internal/utils"
	pb "gitlab.com/xusz/todo/rpc/todo"
)

type simple struct {
	engine *mysql_engine.Engine
}

// New ...
func New(database *gorm.DB) storage.Storage {
	return &simple{
		engine: mysql_engine.New(database),
	}
}

// GetTodo ...
func (s *simple) GetUserTodo(ctx context.Context, req *pb.GetTodoRequest) (*models.TodoView, error) {
	todoView := models.TodoView{}
	err := s.engine.GetUserTodoDB(req.GetUserId(), req.GetId()).Find(&todoView).Error
	if err != nil {
		utils.Logger.Error(err)
		return nil, err
	}

	return &todoView, nil
}
