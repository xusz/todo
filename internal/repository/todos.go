package repository

import (
	"time"

	"github.com/jinzhu/gorm"
	"gitlab.com/xusz/todo/internal/models"
	"gitlab.com/xusz/todo/internal/utils"
)

// Repository ..
type Repository struct {
	DB *gorm.DB
}

// New ...
func New(database *gorm.DB) Repository {
	return Repository{
		DB: database,
	}
}

// IsExistingUserName ...
func (r *Repository) IsExistingUserName(userName string) (*bool, error) {
	isExisting := false
	count := 0
	err := r.DB.Model(&models.User{}).Where("name = ?", userName).Count(&count).Error
	if err != nil {
		utils.Logger.Error(err)
		return nil, err
	}

	if count <= 0 {
		return &isExisting, nil
	}

	isExisting = true

	return &isExisting, nil
}

// IsExistingTodoName ...
func (r *Repository) IsExistingTodoName(todoName, userID string) (*bool, error) {
	isExisting := false
	count := 0
	err := r.DB.Table("todos t").
		Joins("inner join todo_users tu on t.id = tu.todo_id").
		Where("t.name = ? AND tu.user_id = ?", todoName, userID).
		Where("t.deleted_at is null").
		Where("tu.deleted_at is null").Count(&count).Error
	if err != nil {
		utils.Logger.Error(err)
		return nil, err
	}

	if count <= 0 {
		return &isExisting, nil
	}

	isExisting = true

	return &isExisting, nil
}

// GetUserTodo ...
func (r *Repository) GetUserTodo(todoID, userID string) (*models.TodoView, error) {
	var todoView models.TodoView

	err := r.DB.Table("todos t").
		Joins("inner join todo_users tu on t.id = tu.todo_id").
		Joins("inner join users u on tu.user_id = u.id").
		Select("t.*, tu.*, u.*").
		Where("t.id = ? AND tu.user_id = ?", todoID, userID).
		Where("t.deleted_at is null").
		Where("tu.deleted_at is null").
		Scan(&todoView).Error
	if err != nil {
		utils.Logger.Error(err)
		return nil, err
	}

	return &todoView, nil
}

// GetUserTodoCount ...
func (r *Repository) GetUserTodosCount(userID string) (int, error) {
	count := 0
	err := getUserTodosDB(r.DB, userID).
		Count(&count).Error
	if err != nil {
		utils.Logger.Error(err)
		return 0, err
	}

	return count, nil
}

// GetUserTodos for pagination ...
func (r *Repository) GetUserTodos(userID, sortBy string, offset, limit int) ([]models.TodoView, error) {
	todoViews := make([]models.TodoView, 0)

	err := getUserTodosDB(r.DB, userID).
		Order(sortBy).Offset(offset).Limit(limit).
		Scan(&todoViews).Error
	if err != nil {
		utils.Logger.Error(err)
		return nil, err
	}

	return todoViews, nil
}

func getUserTodosDB(db *gorm.DB, userID string) *gorm.DB {
	return db.Table("todos t").
		Joins("inner join todo_users tu on t.id = tu.todo_id").
		Joins("inner join users u on tu.user_id = u.id").
		Select("t.*, tu.*, u.*").
		Where("tu.user_id = ?", userID).
		Where("t.deleted_at is null").
		Where("tu.deleted_at is null")
}

// CreateUser ...
func (r *Repository) CreateUser(user models.User) error {
	err := r.DB.Model(&models.User{}).Create(&user).Error
	if err != nil {
		utils.Logger.Error(err)
		return err
	}

	return nil
}

// CreateTodo ...
func (r *Repository) CreateTodo(todo models.Todo, todoUser models.TodoUser) error {
	tx := r.DB.Begin()
	if tx.Error != nil {
		utils.Logger.Error(tx.Error)
		return tx.Error
	}

	err := tx.Save(&todo).Error
	if err != nil {
		tx.Rollback()
		utils.Logger.Error(err)
		return err
	}

	err = tx.Save(&todoUser).Error
	if err != nil {
		tx.Rollback()
		utils.Logger.Error(err)
		return err
	}

	err = tx.Commit().Error
	if err != nil {
		utils.Logger.Error(err)
		return err
	}

	return nil
}

// UpdateTodo ...
func (r *Repository) UpdateTodo(todo models.Todo, todoUser models.TodoUser) error {
	tx := r.DB.Begin()
	if tx.Error != nil {
		utils.Logger.Error(tx.Error)
		return tx.Error
	}

	// forbidden of using save for update, because of default value
	// err := tx.Save(&todo).Error
	// if err != nil {
	// 	tx.Rollback()
	// 	utils.Logger.Error(err)
	// 	return err
	// }

	// err = tx.Save(&todoUser).Error
	// if err != nil {
	// 	tx.Rollback()
	// 	utils.Logger.Error(err)
	// 	return err
	// }

	// map
	todoMap := make(map[string]interface{})
	todoMap["name"] = todo.Name
	todoMap["description"] = todo.Description
	err := tx.Model(&models.Todo{}).Where("id = ?", todo.ID).Update(todoMap).Error
	if err != nil {
		tx.Rollback()
		utils.Logger.Error(err)
		return err
	}

	todoUserMap := make(map[string]interface{})
	todoUserMap["last_viewed_at"] = todoUser.LastViewedAt
	todoUserMap["last_edited_at"] = todoUser.LastEditedAt
	err = tx.Model(&models.TodoUser{}).
		Where("todo_id = ? and user_id = ?", todoUser.TodoID, todoUser.UserID).
		Update(todoUserMap).Error
	if err != nil {
		tx.Rollback()
		utils.Logger.Error(err)
		return err
	}

	err = tx.Commit().Error
	if err != nil {
		utils.Logger.Error(err)
		return err
	}

	return nil
}

// DeleteTodo ...
func (r *Repository) DeleteTodo(todoID, userID string) error {

	tx := r.DB.Begin()
	if tx.Error != nil {
		utils.Logger.Error(tx.Error)
		return tx.Error
	}

	err := tx.Delete(&models.Todo{}, "id = ?", todoID).Error
	if err != nil {
		tx.Rollback()
		utils.Logger.Error(err)
		return err
	}

	err = tx.Delete(&models.TodoUser{}, "todo_id = ?", todoID).Error
	if err != nil {
		tx.Rollback()
		utils.Logger.Error(err)
		return err
	}

	err = tx.Commit().Error
	if err != nil {
		utils.Logger.Error(err)
		return err
	}

	return nil
}

// SetTodoDone ...
func (r *Repository) SetTodoDone(todoID string) error {
	nowTime := time.Now()
	err := r.DB.Model(&models.Todo{}).Where("id = ?", todoID).Update(models.Todo{IsDone: true, DoneAt: &nowTime}).Error
	if err != nil {
		utils.Logger.Error(err)
		return err
	}

	return nil
}
