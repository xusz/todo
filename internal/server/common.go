package server

import (
	"strconv"
	"strings"

	todo_error "gitlab.com/xusz/todo/internal/error"
	"gitlab.com/xusz/todo/internal/models"
	"gitlab.com/xusz/todo/internal/utils"
	pb "gitlab.com/xusz/todo/rpc/todo"
)

// IsValidName ...
func IsValidName(name string) error {
	if name == "" || strings.Trim(name, " ") == "" {
		return todo_error.ErrorCodeTodoNameNotValid.GetError()
	}

	if strings.Count(name, "")-1 > 255 {
		return todo_error.ErrorCodeTodoNameTooLong.GetError()
	}

	return nil
}

func constructTodoResponse(todoView *models.TodoView) (*pb.Todo, error) {
	if todoView == nil {
		return &pb.Todo{}, nil
	}

	var todoOwnerInfo pb.TodoUserInfo
	todoOwnerInfo.UserId = todoView.UserID
	todoOwnerInfo.UserName = todoView.User.Name
	todoOwnerInfo.Role = todoView.PermissionRole

	todoResponse := &pb.Todo{
		Id:             todoView.Todo.ID,
		Name:           todoView.Todo.Name,
		Description:    todoView.Description,
		LastEditedAt:   strconv.FormatInt(todoView.LastEditedAt.Unix(), 10),
		LastViewedAt:   strconv.FormatInt(todoView.LastViewedAt.Unix(), 10),
		ImportanceName: todoView.Importance.Name,
		IsDone:         todoView.IsDone,
		Owner:          &todoOwnerInfo,
	}

	if todoView.DoneAt != nil {
		// todoResponse.DoneAt = strconv.FormatInt(todoView.DoneAt.Unix(), 10)
		todoResponse.DoneAt = todoView.DoneAt.Format(PATTERN)
	}
	if todoView.DeadLine != nil {
		// todoResponse.DoneAt = strconv.FormatInt(todoView.DoneAt.Unix(), 10)
		todoResponse.DeadLine = todoView.DeadLine.Format(PATTERN)
	}

	return todoResponse, nil
}

func constructTodoConnection(todoViews []models.TodoView, totalCount, offset, limit int) (*pb.TodoConnection, error) {
	todoConnection := new(pb.TodoConnection)
	todoConnection.TotalCount = int32(totalCount)

	var err error
	todoConnection.PageInfo, err = utils.GetPaginationFromData(totalCount, offset, limit)
	if err != nil {
		utils.Logger.Error(err)
		return nil, err
	}

	for i, todoView := range todoViews {
		todoResponse, err := constructTodoResponse(&todoView)
		if err != nil {
			utils.Logger.Error(err)
			return nil, err
		}

		edge := pb.TodoEdge{
			Node:   todoResponse,
			Cursor: utils.Base64Int(offset + i),
		}

		todoConnection.Edges = append(todoConnection.Edges, &edge)
	}

	return todoConnection, nil
}
