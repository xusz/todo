package server

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/golang/protobuf/ptypes/wrappers"
	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/momentum-valley/adam/rpc/common"
	todo_error "gitlab.com/xusz/todo/internal/error"
	"gitlab.com/xusz/todo/internal/repository"

	// "gitlab.com/xusz/todo/internal/storage"
	"gitlab.com/xusz/todo/internal/models"
	"gitlab.com/xusz/todo/internal/utils"
	pb "gitlab.com/xusz/todo/rpc/todo"
)

// PATTERN ...
const PATTERN = "2006-01-02"

// // Server ...
// type Server struct {
// 	Storage storage.Storage
// }

// Server ...
type Server struct {
	Repository repository.Repository
}

// CreateUser ...
func (s *Server) CreateUser(ctx context.Context, req *pb.CreateUserRequest) (*pb.User, error) {
	utils.Logger.Traceln("CreateUser, req:", utils.LogJSONData(req))

	if req == nil {
		return nil, todo_error.ErrorCodeInputParameterNullError.GetError()
	}

	err := IsValidName(req.Name)
	if err != nil {
		return nil, err
	}
	toInsertUserName := strings.Trim(req.Name, " ")

	// check no user with the same name
	isExisting, err := s.Repository.IsExistingUserName(toInsertUserName)
	if err != nil {
		utils.Logger.Error(err)
		return nil, err
	}
	if isExisting != nil && *isExisting {
		utils.Logger.Errorf("User name[%s] already exists", toInsertUserName)
		return nil, todo_error.ErrorCodeUserNameAlreadyExistence.GetError()
	}

	var toInsertUser = models.User{
		BasicModel: models.BasicModel{
			ID: uuid.NewV4().String(),
		},
		Name: req.GetName(),
	}

	err = s.Repository.CreateUser(toInsertUser)
	if err != nil {
		utils.Logger.Error(err)
		return nil, todo_error.ErrorCodeDataBaseWrong.GetError()
	}

	responseUser := &pb.User{
		Id:   toInsertUser.ID,
		Name: toInsertUser.Name,
	}

	return responseUser, nil
}

// GetTodos ...
func (s *Server) GetTodos(ctx context.Context, req *pb.GetTodosRequest) (*pb.TodoConnection, error) {

	todoConnection := new(pb.TodoConnection)
	todoConnection.PageInfo = new(common.PageInfo)

	offset, limit, err := utils.GetOffsetAndLimit(req.First, req.Last, req.Before, req.After)
	sortBy := req.SortBy.String()
	if req.IsDesc {
		sortBy += " desc"
	}

	// get totalCount
	totalCount, err := s.Repository.GetUserTodosCount(req.UserId)
	if err != nil {
		return nil, todo_error.ErrorCodeDataBaseWrong.GetError()
	}
	if totalCount <= 0 {
		utils.Logger.Infof("No todo data with userID [%v]", req.UserId)
		return todoConnection, nil
	}
	utils.Logger.Traceln("GetEntityCollections, totalCount: ", totalCount)

	// get detail data
	todoViews, err := s.Repository.GetUserTodos(req.UserId, sortBy, offset, limit)
	if err != nil {
		return nil, todo_error.ErrorCodeDataBaseWrong.GetError()
	}
	if todoViews == nil || len(todoViews) <= 0 {
		utils.Logger.Infof("No todo data with userID [%v]", req.UserId)
		return todoConnection, nil
	}

	// make response
	todoConnection, err = constructTodoConnection(todoViews, totalCount, offset, limit)
	if err != nil {
		utils.Logger.Error(err)
		return nil, todo_error.ErrorCodeSystemWrong.GetError()
	}
	utils.Logger.Traceln("GetTodos, todoConnection: ", utils.LogJSONData(todoConnection))

	return todoConnection, nil
}

// GetTodo ...
func (s *Server) GetTodo(ctx context.Context, req *pb.GetTodoRequest) (*pb.Todo, error) {
	utils.Logger.Traceln("GetTodo, req:", utils.LogJSONData(req))

	if req == nil {
		return nil, todo_error.ErrorCodeInputParameterNullError.GetError()
	}

	// query
	todoView, err := s.Repository.GetUserTodo(req.Id, req.UserId)
	if gorm.IsRecordNotFoundError(err) {
		return nil, todo_error.ErrorCodeDataNotExistence.GetError()
	}
	if err != nil {
		return nil, todo_error.ErrorCodeDataBaseWrong.GetError()
	}
	utils.Logger.Traceln("GetTodo, todoView:", utils.LogJSONData(todoView))

	// make response
	todoResponse, err := constructTodoResponse(todoView)
	if err != nil {
		utils.Logger.Error(err)
		return nil, todo_error.ErrorCodeDataBaseWrong.GetError()
	}
	utils.Logger.Traceln("GetTodo, TodoResponse: ", utils.LogJSONData(todoResponse))

	return todoResponse, nil
}

// CreateTodo ...
// Check input parameter nog null, check todo name not empty and not too long
func (s *Server) CreateTodo(ctx context.Context, req *pb.CreateTodoRequest) (*pb.Todo, error) {
	utils.Logger.Traceln("CreateTodo, req:", utils.LogJSONData(req))

	if req == nil {
		return nil, todo_error.ErrorCodeInputParameterNullError.GetError()
	}

	err := IsValidName(req.Name)
	if err != nil {
		return nil, err
	}

	toInsertTodoName := strings.Trim(req.Name, " ")

	// check no todo with the same name
	isExisting, err := s.Repository.IsExistingTodoName(toInsertTodoName, req.UserId)
	if err != nil {
		return nil, todo_error.ErrorCodeDataBaseWrong.GetError()
	}
	if isExisting != nil && *isExisting {
		utils.Logger.Errorf("Todo name[%s] already exists", toInsertTodoName)
		return nil, todo_error.ErrorCodeTodoNameExistence.GetError()
	}

	// create Todo
	nowTime := time.Now()
	deadLine, err := time.Parse(PATTERN, req.GetDeadLine())
	if err != nil {
		utils.Logger.Error(err)
		return nil, todo_error.ErrorCodeSystemWrong.GetError()
	}
	fmt.Println("deadLine: ", deadLine)
	insertTodo := models.Todo{
		BasicModel: models.BasicModel{
			ID: uuid.NewV4().String(),
		},
		Name:        toInsertTodoName,
		Description: req.GetDescription(),
		DeadLine:    &deadLine,
	}
	insertTodoUser := models.TodoUser{
		TodoID:         insertTodo.BasicModel.ID,
		UserID:         req.UserId,
		PermissionRole: pb.TodoPermissionRole_OWNER,

		CreatedAt:    nowTime,
		UpdatedAt:    nowTime,
		LastViewedAt: nowTime,
		LastEditedAt: nowTime,
	}

	err = s.Repository.CreateTodo(insertTodo, insertTodoUser)
	if err != nil {
		return nil, todo_error.ErrorCodeDataBaseWrong.GetError()
	}

	// construct response
	var todoView = models.TodoView{
		Todo:     insertTodo,
		TodoUser: insertTodoUser,
	}
	todoResponse, err := constructTodoResponse(&todoView)
	if err != nil {
		utils.Logger.Error(err)
		return nil, todo_error.ErrorCodeDataBaseWrong.GetError()
	}
	utils.Logger.Traceln("CreateTodo, TodoResponse: ", utils.LogJSONData(todoResponse))

	return todoResponse, nil
}

// UpdateTodo ...
func (s *Server) UpdateTodo(ctx context.Context, req *pb.UpdateTodoRequest) (*pb.Todo, error) {
	utils.Logger.Traceln("UpdateTodo, req:", utils.LogJSONData(req))

	if req == nil {
		return nil, todo_error.ErrorCodeInputParameterNullError.GetError()
	}

	// check existing
	todoView, err := s.Repository.GetUserTodo(req.Id, req.UserId)
	if gorm.IsRecordNotFoundError(err) {
		return nil, todo_error.ErrorCodeDataNotExistence.GetError()
	}
	if err != nil {
		return nil, todo_error.ErrorCodeDataBaseWrong.GetError()
	}
	utils.Logger.Traceln("UpdateTodo, todoView:", utils.LogJSONData(todoView))

	err = IsValidName(req.Name)
	if err != nil {
		return nil, err
	}
	toUpdateTodoName := strings.Trim(req.Name, " ")

	// update Todo
	nowTime := time.Now()
	todoView.Todo.Name = toUpdateTodoName
	todoView.Todo.Description = req.GetDescription()
	todoView.TodoUser.LastViewedAt = nowTime
	todoView.TodoUser.LastEditedAt = nowTime

	if req.DeadLine != "" {
		deadLine, err := time.Parse(PATTERN, req.DeadLine)
		if err != nil {
			utils.Logger.Error(err)
			return nil, todo_error.ErrorCodeSystemWrong.GetError()
		}
		todoView.Todo.DeadLine = &deadLine
	}
	err = s.Repository.UpdateTodo(todoView.Todo, todoView.TodoUser)
	if err != nil {
		return nil, todo_error.ErrorCodeDataBaseWrong.GetError()
	}

	// make response
	todoResponse, err := constructTodoResponse(todoView)
	if err != nil {
		utils.Logger.Error(err)
		return nil, todo_error.ErrorCodeDataBaseWrong.GetError()
	}
	utils.Logger.Traceln("UpdateTodo, TodoResponse: ", utils.LogJSONData(todoResponse))

	return todoResponse, nil
}

// DeleteTodo ...
func (s *Server) DeleteTodo(ctx context.Context, req *pb.DeleteTodoRequest) (*wrappers.BoolValue, error) {
	utils.Logger.Traceln("DeleteTodo, req:", utils.LogJSONData(req))

	if req == nil {
		return nil, todo_error.ErrorCodeInputParameterNullError.GetError()
	}

	// check
	todoView, err := s.Repository.GetUserTodo(req.Id, req.UserId)
	if gorm.IsRecordNotFoundError(err) {
		return nil, todo_error.ErrorCodeDataNotExistence.GetError()
	}
	if err != nil {
		return nil, todo_error.ErrorCodeDataBaseWrong.GetError()
	}
	utils.Logger.Traceln("DeleteTodo, todoView:", utils.LogJSONData(todoView))

	// delete
	err = s.Repository.DeleteTodo(req.Id, req.UserId)
	if err != nil {
		return nil, todo_error.ErrorCodeDataBaseWrong.GetError()
	}

	return &wrappers.BoolValue{Value: true}, nil
}

// SetTodoDone ...
func (s *Server) SetTodoDone(ctx context.Context, req *pb.SetTodoDoneRequest) (*wrappers.BoolValue, error) {
	utils.Logger.Traceln("SetTodoDone, req:", utils.LogJSONData(req))

	if req == nil {
		return nil, todo_error.ErrorCodeInputParameterNullError.GetError()
	}

	// check
	todoView, err := s.Repository.GetUserTodo(req.Id, req.UserId)
	if gorm.IsRecordNotFoundError(err) {
		return nil, todo_error.ErrorCodeDataNotExistence.GetError()
	}
	if err != nil {
		return nil, todo_error.ErrorCodeDataBaseWrong.GetError()
	}
	utils.Logger.Traceln("SetTodoDone, todoView:", utils.LogJSONData(todoView))

	err = s.Repository.SetTodoDone(req.Id)
	if err != nil {
		return nil, todo_error.ErrorCodeDataBaseWrong.GetError()
	}

	return &wrappers.BoolValue{Value: true}, nil
}
