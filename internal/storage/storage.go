package storage

import (
	"context"

	"gitlab.com/xusz/todo/internal/models"
	pb "gitlab.com/xusz/todo/rpc/todo"
)

// Storage ...
type Storage interface {
	GetUserTodo(ctx context.Context, req *pb.GetTodoRequest) (*models.TodoView, error)
}
