package models

import (
	"time"

	pb "gitlab.com/xusz/todo/rpc/todo"
)

// BasicModel ...
type BasicModel struct {
	ID        string     `gorm:"type:VARCHAR(100);primary_key"`
	CreatedAt time.Time  `gorm:"not null"`
	UpdatedAt time.Time  `gorm:"not null"`
	DeletedAt *time.Time `gorm:"index"`
}

// Todo ...
type Todo struct {
	BasicModel

	Name        string `gorm:"type:VARCHAR(255);not null"`
	Description string `gorm:"type:VARCHAR(500);not null"`
	IsDone      bool   `gorm:"type:INT;not null"`
	DeadLine    *time.Time
	DoneAt      *time.Time
}

// User ...
type User struct {
	BasicModel

	Name  string  `gorm:"type:VARCHAR(100);not null"`
	Phone *string `gorm:"type:varchar(11);unique_index;"`
	Email *string `gorm:unique_index;"`
}

// TodoUser ...
type TodoUser struct {
	TodoID         string                `gorm:"type:VARCHAR(100);primary_key"`
	UserID         string                `gorm:"type:VARCHAR(100);primary_key"`
	PermissionRole pb.TodoPermissionRole `gorm:"type:INT;not null"`

	LastViewedAt time.Time
	LastEditedAt time.Time

	CreatedAt time.Time  `gorm:"not null"`
	UpdatedAt time.Time  `gorm:"not null"`
	DeletedAt *time.Time `gorm:"index"`
}

// TodoImportance ...
type TodoImportance struct {
	TodoID       string `gorm:"type:VARCHAR(100);primary_key"`
	ImportanceID string `gorm:"type:VARCHAR(100);primary_key"`

	CreatedAt time.Time  `gorm:"not null"`
	UpdatedAt time.Time  `gorm:"not null"`
	DeletedAt *time.Time `gorm:"index"`
}

// Importance ...
type Importance struct {
	BasicModel

	Name         string `gorm:"type:VARCHAR(255);not null"`
	RankingScore int    `gorm:"type:INT;not null"`
}

// TableName ...
func (Importance) TableName() string {
	return "importance"
}
