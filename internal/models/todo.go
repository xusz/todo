package models

import (
	"time"

	pb "gitlab.com/xusz/todo/rpc/todo"
)

// TodoUserView used for `UserName` instead of db model
type TodoUserView struct {
	TodoID         string
	UserID         string
	UserName       string
	PermissionRole pb.TodoPermissionRole

	LastViewedAt *time.Time
	LastEditedAt *time.Time

	CreatedAt time.Time
}

// TodoView ...
type TodoView struct {
	Todo
	TodoUser
	User
	Importance
}
