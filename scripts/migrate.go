package main

import (
	"flag"
	"fmt"
	"strings"

	"gitlab.com/xusz/todo/internal/models"
	"gitlab.com/xusz/todo/internal/utils"
)

var configFilename string
var configDirs string

func init() {
	const (
		defaultConfigFilename = "config"
		configUsage           = "Name of the config file, without extension"
		defaultConfigDirs     = ".,./configs/"
		configDirUsage        = "Directories to search for config file, separated by ','"
	)
	flag.StringVar(&configFilename, "c", defaultConfigFilename, configUsage)
	flag.StringVar(&configDirs, "p", defaultConfigDirs, configDirUsage)
}

func main_1() {
	flag.Parse()

	err := utils.NewConfiguration(configFilename, strings.Split(configDirs, ","))
	if err != nil {
		panic(fmt.Errorf("Error parsing config, %s", err))
	}

	db, err := utils.ConnectToDatabase(utils.CONFIG)
	if err != nil {
		utils.Logger.Fatal(err)
	}

	defer db.Close()

	utils.Logger.Info("migrating tables...")

	// migrate tables
	db.AutoMigrate(
		&models.Todo{},
		&models.User{},
		&models.Importance{},
		&models.TodoUser{},
		&models.TodoImportance{},
	)

	// set foreignKeys
	db.Model(&models.TodoUser{}).AddForeignKey("todo_id", "todos(id)", "CASCADE", "CASCADE")
	db.Model(&models.TodoUser{}).AddForeignKey("user_id", "users(id)", "CASCADE", "CASCADE")

	db.Model(&models.TodoImportance{}).AddForeignKey("todo_id", "todos(id)", "CASCADE", "CASCADE")
	db.Model(&models.TodoImportance{}).AddForeignKey("importance_id", "importance(id)", "CASCADE", "CASCADE")

	utils.Logger.Info("finish tables migration!")
}
