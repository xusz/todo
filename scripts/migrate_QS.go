package main

// 导入依赖包
import (
	"fmt"
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// Product ...
type Product struct {
	ID        uint      //`gorm:"PRIMARY_KEY"`
	Name      string    `gorm:"type:VARCHAR(255);not null"`
	Price     uint      `gorm:"type:INT;not null"`
	CreatedAt time.Time `gorm:"not null"`
}

func main() {

	// 连接数据库，格式 `userName:password@(host:3306)/dbName?charset=utf8&parseTime=True&loc=Local&multiStatements=True`
	dbConnMysql := `root:pacman@(127.0.0.1:3306)/todo?charset=utf8&parseTime=True&multiStatements=True`
	db, _ := gorm.Open("mysql", dbConnMysql)

	db.LogMode(true)

	// 自动创建表格，默认复数形式
	db.AutoMigrate(&Product{})

	// 添加一条记录
	db.Create(&Product{Name: "冰箱", Price: 1000, CreatedAt: time.Now()})
	db.Exec("insert into products(id, name, price, created_at) values(6, '洗衣机', 2000, now())")

	// 读取一条数据
	var product Product
	db.First(&product, "Name = ?", "冰箱")
	fmt.Printf("%+v \n", product)

	// db.Model(&product).Update("Price", 2000)

	// db.Delete(&product)
}
