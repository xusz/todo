package main

//go:generate rm -f ./rpc/todo/service.twirp.go
//go:generate rm -f ./rpc/todo/service.pb.go
//go:generate protoc --proto_path=./vendor/:. --twirp_out=. --go_out=. ./rpc/todo/service.proto
