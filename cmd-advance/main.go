package main

import (
	"flag"
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/xusz/todo/internal/server"
	"gitlab.com/xusz/todo/internal/utils"
	pb "gitlab.com/xusz/todo/rpc/todo"
)

var configFilename string
var configDirs string

func init() {
	const (
		defaultConfigFilename = "config"
		configUsage           = "Name of the config file, without extension"
		defaultConfigDirs     = ".,./configs/"
		configDirUsage        = "Directories to search for config file, separated by ','"
	)
	flag.StringVar(&configFilename, "c", defaultConfigFilename, configUsage)
	flag.StringVar(&configDirs, "p", defaultConfigDirs, configDirUsage)
}

func main() {
	flag.Parse()

	err := utils.NewConfiguration(configFilename, strings.Split(configDirs, ","))
	if err != nil {
		panic(fmt.Errorf("Error parsing config, %s", err))
	}

	utils.InitLogger()
	if utils.CONFIG.DebugMode {
		utils.Logger.Info("Prepare running on DebugMode...")
	}

	server := newServer()
	twirpHandler := pb.NewTodoServiceServer(server, nil)
	utils.Logger.Infoln("service on ...")
	http.ListenAndServe(":"+utils.CONFIG.Service.Port, twirpHandler)
}

func newServer() *server.Server {
	db, err := utils.ConnectToDatabase(utils.CONFIG)
	if err != nil {
		utils.Logger.Fatal(err)
	}
	if utils.CONFIG.DB.LogMode {
		db.LogMode(utils.CONFIG.DB.LogMode)
		utils.Logger.Info("Prepare running on LogMode...")
	}

	server := &server.Server{
		// Storage: simple.New(db),
	}

	return server
}
